const form = document.getElementById("form");
const input = document.getElementById("input");
const messages = document.getElementById("messages");

form.addEventListener("submit", async (event) => {
  event.preventDefault();
  const question = input.value;
  input.value = "";
  addMessage(question, false);

  const answer = await askDogExpert(question);
  addMessage(answer, true);
});

function addMessage(text, isBot) {
  const message = document.createElement("div");
  message.classList.add("message");
  message.classList.add(isBot ? "bot" : "user");
  message.innerText = text;
  messages.appendChild(message);
  messages.scrollTop = messages.scrollHeight;
}

async function askDogExpert(question) {
  const response = await fetch("/api/chat", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ question }),
  });

  const data = await response.json();
  return data.answer;
}
